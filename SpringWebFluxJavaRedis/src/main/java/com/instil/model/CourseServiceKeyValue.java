package com.instil.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class CourseServiceKeyValue {
    private ReactiveRedisConnectionFactory factory;
    private ReactiveRedisOperations<String, Course> courseOps;
    private List<Course> portfolio;

    @Autowired
    public CourseServiceKeyValue(ReactiveRedisConnectionFactory factory,
                                 ReactiveRedisOperations<String, Course> courseOps,
                                 List<Course> portfolio) {
        this.factory = factory;
        this.courseOps = courseOps;
        this.portfolio = portfolio;
    }

    public Flux<Boolean> populate() {
        return factory.getReactiveConnection()
                .serverCommands()
                .flushAll()
                .thenMany(Flux
                        .fromIterable(portfolio)
                        .flatMap(c -> courseOps.opsForValue().set(buildKey(c.getId()), c)));
    }

    //Building custom key identifier
    private String buildKey(String courseId) {
        return String.format("courses-manual:%s", courseId);
    }

    public Flux<Course> get() {
        return courseOps.keys("courses-manual:*")
                .flatMap(courseOps.opsForValue()::get);
    }

    public Mono<Long> delete(String id) {
        return courseOps.delete(buildKey(id));
    }

    public Mono<Course> get(String id) {
        return courseOps.opsForValue().get(buildKey(id));
    }
}

