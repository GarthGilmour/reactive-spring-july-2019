package com.instil.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//ReactiveCrudRepository are not supported by Redis
@Repository
public interface CourseRepository extends CrudRepository<Course, String> {
    //Complex queries such as Contains are not supported with Redis
    Iterable<Course> findByTitleIgnoreCase(String title);
}