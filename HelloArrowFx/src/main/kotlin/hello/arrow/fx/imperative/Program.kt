package hello.arrow.fx.imperative

import java.util.*


fun printMenu() = println(
    """
        Choose an option:
        [Add] a string to the list
        [Get] a string at a specified position
        [Random] a string at a Random position
        [Remove] a string from a specified position
        [Print] the current contents of the list
        [Quit] the program
      """.trimIndent()
)

fun main() {
    val random = Random()
    val list = LinkedList<String>()
    var exec = true
    while (exec) {
        printMenu()

        when (val choice = readLine()?.toLowerCase()) {
            "add" -> {
                println("Enter a string to Add")
                val item = readLine() ?: ""
                list.add(item)
            }
            "get" -> {
                println("Enter an index")
                val index = readLine()?.toInt() ?: -1
                println("item at $index is ${list[index]}")
            }
            "random" -> {
                val index = random.nextInt(list.size)
                println("Random item at $index is ${list[index]}")
            }
            "remove" -> {
                println("Enter an index")
                val index = readLine()?.toInt() ?: -1
                println("Item ${list.removeAt(index)} removed from position $index")
            }
            "print" -> {
                println("Contents of list are: ${list.map { "\n\t $it" }}")
            }
            "quit" -> {
                println("Thanks for playing!")
                exec = false
            }
            else -> {
                println("Don't understand $choice")
            }
        }
    }
}