package hello.arrow.fx.effectful

import arrow.effects.IO
import arrow.effects.extensions.io.fx.fx
import arrow.effects.extensions.io.unsafeRun.runBlocking
import arrow.effects.handleErrorWith
import arrow.unsafe
import java.util.Random as utilRandom


sealed class Command {
  companion object {
    operator fun invoke(cmd: String) = when (cmd.trim().toLowerCase()) {
      "add" -> Add
      "get" -> Get
      "random" -> Random
      "remove" -> Remove
      "print" -> Print
      "quit" -> Quit
      else -> Unknown
    }
  }
}

data class ConsoleError(val msg: String) : Throwable(msg)
object Add : Command()
object Get : Command()
object Random : Command()
object Print : Command()
object Remove : Command()
object Quit : Command()
object Unknown : Command()


object effects {

  fun putStrLn(s: String): IO<Unit> = IO { println(s) }

  fun getStrLn(): IO<String> = IO { readLine() ?: "" }

  fun nextInt(upper: Int): IO<Int> = IO { utilRandom().nextInt(upper) }

  fun parseInt(s: String): IO<Int> =
    IO { s.trim().toInt() }
      .handleErrorWith { IO.raiseError(ConsoleError("$s is not a number")) }

  fun readInteger(): IO<Int> = fx {
    val input = !getStrLn()
    !parseInt(input)
  }
  // or getStrLn().flatMap { input -> parseInt(input) }


  fun <A> List<A>.safeGet(idx: Int): IO<A> = IO { this[idx] }

  fun printMenu(): IO<Unit> = putStrLn(
    """
      Choose an option:
      [Add] a string to the list
      [Get] a string at a specified position
      [Random] a string at a Random position
      [Remove] a string from a specified position
      [Print] the current contents of the list
      [Quit] the program
           """.trimIndent()
  )

  fun commandLoop(state: List<String>): IO<Unit> = fx {
    !printMenu()
    val choice = !getStrLn()
    val command = Command(choice)
    val newState = !processCommand(command, state)
    if (command is Quit) Unit
    else !commandLoop(newState)
  }.handleErrorWith { t ->
    fx {
      !putStrLn(t.message ?: "unknown error")
      !commandLoop(state)
    }
  }


  fun commandLoopNested(state: List<String>): IO<Unit> =
    printMenu().flatMap {
      getStrLn().flatMap { choice ->
        val command = Command(choice)
        processCommand(command, state).flatMap { newState ->
          if (command is Quit) IO.unit
          else commandLoopNested(newState)
        }

      }
    }.handleErrorWith { t ->
      putStrLn(t.message ?: "unknown error")
        .flatMap { commandLoop(state) }
    }


  fun processCommand(cmd: Command, state: List<String>): IO<List<String>> = fx {
    when (cmd) {
      Add -> {
        !putStrLn("Enter a string to Add")
        val item = !getStrLn()
        state + item
      }
      Get -> {
        !putStrLn("Enter an index")
        val index = !readInteger()
        val elem = !state.safeGet(index)
        !putStrLn("Item at $index is $elem")
        state
      }
      Random -> {
        val rndIdx = !nextInt(state.size)
        val elem = !state.safeGet(rndIdx)
        !putStrLn("Random item at $rndIdx is $elem")
        state
      }
      Print -> {
        !putStrLn("Contents of list are: $state")
        state
      }
      Remove -> {
        !putStrLn("Enter an index")
        val index = !getStrLn()
        val idx = !parseInt(index)
        val toRemove = !state.safeGet(idx)

        !putStrLn("Item $toRemove removed from position $idx")
        state.removeAt(idx)
      }
      Quit -> {
        !putStrLn("Thanks for playing!")
        state
      }
      Unknown -> {
        !putStrLn("Unknown command")
        state
      }
    }
  }
}

fun <A> List<A>.removeAt(idx: Int): List<A> {
  val bfrIdx = this.filterIndexed { i, _ -> i < idx }
  val aftIdx = this.filterIndexed { i, _ -> i > idx }
  return bfrIdx + aftIdx
}

fun main() {
  unsafe {
    runBlocking { effects.commandLoop(emptyList()) }
  }
}