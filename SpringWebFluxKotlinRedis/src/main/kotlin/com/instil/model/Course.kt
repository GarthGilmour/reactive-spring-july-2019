package com.instil.model

import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash

enum class CourseDifficulty {
    BEGINNER,
    INTERMEDIATE,
    ADVANCED
}

@RedisHash("courses")
data class Course(@Id var id: String = "",
                  var title: String = "",
                  var difficulty: CourseDifficulty = CourseDifficulty.BEGINNER,
                  var duration: Int = 0)