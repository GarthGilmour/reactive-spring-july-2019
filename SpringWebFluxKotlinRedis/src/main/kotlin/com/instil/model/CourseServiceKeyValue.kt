package com.instil.model

import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class CourseServiceKeyValue(val factory: ReactiveRedisConnectionFactory,
                            val courseOps: ReactiveRedisOperations<String, Course>,
                            val demoCourses: List<Course>) {
    fun populate(): Flux<Boolean> {
        return factory.reactiveConnection.serverCommands().flushAll().thenMany(
                Flux.fromIterable(demoCourses)
                        .flatMap { courseOps.opsForValue().set(buildKey(it.id), it) })
    }

    //Building custom key identifier
    private fun buildKey(courseId: String) = "courses-manual:${courseId}"

    fun get(): Flux<Course> {
        return courseOps.keys("courses-manual:*")
                .flatMap(courseOps.opsForValue()::get)
    }

    fun delete(id: String): Mono<Long> {
        return courseOps.delete(buildKey(id))
    }

    fun get(id: String): Mono<Course> {
        return courseOps.opsForValue().get(buildKey(id))
    }
}
