package com.instil

import com.instil.model.Course
import com.instil.model.CourseDifficulty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.core.ReactiveRedisTemplate
import org.springframework.data.redis.serializer.RedisSerializationContext
import org.springframework.data.redis.serializer.StringRedisSerializer
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories

@Configuration
@EnableRedisRepositories
class ApplicationConfig {
    @Bean
    internal fun redisOperations(factory: ReactiveRedisConnectionFactory): ReactiveRedisOperations<String, Course> {
        val serializer = Jackson2JsonRedisSerializer(Course::class.java)

        val builder = RedisSerializationContext.newSerializationContext<String, Course>(StringRedisSerializer())

        val context = builder.value(serializer).build()

        return ReactiveRedisTemplate(factory, context)
    }

    @Bean
    internal fun stringOperations(factory: ReactiveRedisConnectionFactory): ReactiveRedisOperations<String, String> {
        return ReactiveRedisTemplate(factory, RedisSerializationContext.string())
    }

    @Bean
    internal fun demoCourses() = listOf(
            Course("AB12", "Programming in Scala", CourseDifficulty.BEGINNER, 4),
            Course("CD34", "Machine Learning in Python", CourseDifficulty.INTERMEDIATE, 3),
            Course("EF56", "Advanced Kotlin Coding", CourseDifficulty.ADVANCED, 2),
            Course("GH78", "Intro to Domain Driven Design", CourseDifficulty.BEGINNER, 3),
            Course("IJ90", "Database Access with JPA", CourseDifficulty.INTERMEDIATE, 3),
            Course("KL12", "Functional Design Patterns in F#", CourseDifficulty.ADVANCED, 2),
            Course("MN34", "Building Web UIs with Angular", CourseDifficulty.BEGINNER, 4),
            Course("OP56", "Version Control with Git", CourseDifficulty.INTERMEDIATE, 1),
            Course("QR78", "SQL Server Masterclass", CourseDifficulty.ADVANCED, 2),
            Course("ST90", "Go Programming for Beginners", CourseDifficulty.BEGINNER, 5),
            Course("UV12", "Coding with Lock Free Algorithms", CourseDifficulty.INTERMEDIATE, 2),
            Course("WX34", "Coaching Skills for SCRUM Masters", CourseDifficulty.ADVANCED, 3)
    )

}